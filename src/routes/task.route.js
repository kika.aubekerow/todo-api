const router = require('express').Router()
const {
  createTaskController,
  deleteTaskController,
  doneTaskController,
  putTaskController,
  getTaskListController,
  getDoneListController,
} = require('../controllers/task.controller')
const { JoiValidateRequestSchema, createTaskSchema, doneTaskSchema } = require('../middlewares/joi.middleware')

router.post('/tasks', JoiValidateRequestSchema(createTaskSchema), createTaskController)
router.get('/tasks', getTaskListController)
router.post('/tasks/done', JoiValidateRequestSchema(doneTaskSchema), doneTaskController)
router.get('/tasks/done', getDoneListController)

router.put('/tasks/:taskTitle', putTaskController)
router.delete('/tasks/', JoiValidateRequestSchema(doneTaskSchema), deleteTaskController)

module.exports = router
