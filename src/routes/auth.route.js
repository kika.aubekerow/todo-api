const router = require('express').Router()

const { loginUserController, registerUserController } = require('../controllers/auth.controller')
const { JoiValidateRequestSchema, loginUserSchema } = require('../middlewares/joi.middleware')

router.post('/login', JoiValidateRequestSchema(loginUserSchema), loginUserController)
router.post('/register', JoiValidateRequestSchema(loginUserSchema), registerUserController)

module.exports = router
