const mongoose = require('mongoose')
const bcrypt = require('bcrypt')

const UserSchema = new mongoose.Schema({
  email: {
    type: String,
    unique: true,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  firstName: {
    type: String,
    required: true,
  },
  lastName: {
    type: String,
    required: true,
  },
})

UserSchema.methods.isPassValid = async function isPassValid(password) {
  return bcrypt.compare(password, this.password)
}

const SALT_ROUND = 10

UserSchema.pre('save', async function preSave(next) {
  if (!this.isModified('password')) return next()
  const unhashPass = this.password
  this.password = await bcrypt.hash(unhashPass, SALT_ROUND)
  return next()
})

module.exports = mongoose.model('User', UserSchema)
