const mongoose = require('mongoose')
const config = require('../config/index')

const connectMongo = async () => {
  await mongoose.connect(config.mongoURI)
}

module.exports = connectMongo
