const {
  send404, ERROR_CODE, send400, send500,
} = require('../config/error.config')

const {
  addTask, getTaskList, removeTask, markTaskDone,
  changeTask,
} = require('../services/task.service')

const createTaskController = async (req, res) => {
  try {
    const { title, description } = req.body
    const { id } = req.user
    const addedTask = await addTask(id, title, description)

    if (!addedTask) {
      return send404(res)
    }

    return res.status(201).json(addedTask)
  } catch (err) {
    if (err.code === 11000) {
      return send400(res, 'error', ERROR_CODE.TASK_ALREADY_EXIST)
    }

    return send500(err, res)
  }
}

const deleteTaskController = async (req, res) => {
  const { title } = req.body
  const { id } = req.user
  const addedTask = await removeTask(id, title)

  if (!addedTask) {
    return send404(res)
  }

  return res.json(addedTask)
}

const doneTaskController = async (req, res) => {
  try {
    const { id } = req.user
    const { title } = req.body

    const task = await markTaskDone(id, title)

    if (!task) {
      return send404(res)
    }

    return res.json(task)
  } catch (err) {
    if (err.message === ERROR_CODE.TASK_ALREADY_DONE) {
      return send400(res, 'error', ERROR_CODE.TASK_ALREADY_DONE)
    }

    return send500(err, res)
  }
}

const putTaskController = async (req, res) => {
  try {
    const { id } = req.user
    const { taskTitle } = req.params

    const { title, description } = req.body
    const task = await changeTask(id, taskTitle, title, description)

    if (!task) {
      return send404(res)
    }

    return res.json(task)
  } catch (err) {
    if (err.message === ERROR_CODE.TASK_ALREADY_DONE) {
      return send400(res, 'error', ERROR_CODE.TASK_ALREADY_DONE)
    }

    if (err.message === ERROR_CODE.TITLE_NOT_UNIQUE) {
      return send400(res, 'error', ERROR_CODE.TITLE_NOT_UNIQUE)
    }

    return send500(err, res)
  }
}

const getTaskListController = async (req, res) => {
  const { id } = req.user
  const taskList = await getTaskList(id)
  return res.json(taskList)
}

const getDoneListController = async (req, res) => {
  const { id } = req.user
  const taskList = await getTaskList(id, true)
  return res.json(taskList)
}

module.exports = {
  createTaskController,
  deleteTaskController,
  doneTaskController,
  putTaskController,
  getTaskListController,
  getDoneListController,
}
