const jwt = require('jsonwebtoken')

const {
  ERROR_CODE, send400, send500,
} = require('../config/error.config')
const config = require('../config')
const { createUser, getUser } = require('../services/user.service')

const loginUserController = async (req, res) => {
  try {
    const {
      email: reqEmail, password: reqPassword,
    } = req.body

    const user = await getUser(reqEmail, reqPassword)

    if (!user) {
      return res.status(404).json()
    }

    const { email, firstName, lastName } = user

    const token = jwt.sign({
      email, firstName, lastName,
    }, config.jwtSecret, { expiresIn: '1h' })

    return res.status(200).json(token)
  } catch (err) {
    if (err.message === ERROR_CODE.PASS_NOT_VALID) {
      return send400(res, 'error', ERROR_CODE.PASS_NOT_VALID)
    }

    if (err.message === ERROR_CODE.USER_ALREADY_EXIST) {
      return send400(res, 'error', ERROR_CODE.USER_ALREADY_EXIST)
    }

    return send500(err, res)
  }
}

const registerUserController = async (req, res) => {
  try {
    const {
      email, password, firstName, lastName,
    } = req.body

    const user = await createUser(email, password, firstName, lastName)

    return res.json(user)
  } catch (err) {
    if (err.code === 11000) {
      return send400(res, 'error', ERROR_CODE.USER_ALREADY_EXIST)
    }
    return send500(err, res)
  }
}

module.exports = {
  loginUserController,
  registerUserController,
}
