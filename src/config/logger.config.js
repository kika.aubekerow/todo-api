const winston = require('winston')

const { timestamp, errors, json } = winston.format

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(timestamp(), errors({ stack: true }), json()),
  transports: [
    new winston.transports.File({ filename: 'log/error', level: 'error' }),
    new winston.transports.File({ filename: 'log/logs' }),
  ],
})

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }))
}

module.exports = logger
