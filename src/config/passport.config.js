const passport = require('passport')
const { Strategy, ExtractJwt } = require('passport-jwt')
const UserModel = require('../models/user.model')
const config = require('.')

const initPassport = () => {
  const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.jwtSecret,
  }

  passport.use(new Strategy(jwtOptions, async (jwtPayload, done) => {
    const user = await UserModel.findOne({ email: jwtPayload.email })

    if (user) {
      const { _id, firstName, lastName } = user
      done(null, { id: _id, firstName, lastName })
    } else {
      done(null, false)
    }
  }))
}

module.exports = initPassport
