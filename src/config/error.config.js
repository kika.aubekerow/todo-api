const logger = require('./logger.config')

const ERROR_CODE_FIELD = 'errorCode'

const ERROR_CODE = {
  PASS_NOT_VALID: 'PASS_NOT_VALID',
  USER_ALREADY_EXIST: 'USER_ALREADY_EXIST',
  TASK_ALREADY_EXIST: 'TASK_ALREADY_EXIST',
  TASK_ALREADY_DONE: 'TASK_ALREADY_DONE',
  UNAUTHORIZED: 'UNAUTHORIZED',
  NOT_FOUND: 'NOT_FOUND',
  TITLE_NOT_UNIQUE: 'TITLE_NOT_UNIQUE',
}

const send200 = (res) => res.status(200).json({})

const send400 = (res, field, errCode) => res.status(400).json({ [field]: errCode })

const send401 = (res) => res.status(401).json({ [ERROR_CODE_FIELD]: ERROR_CODE.UNAUTHORIZED })

const send404 = (res) => res.status(404).json({ [ERROR_CODE_FIELD]: ERROR_CODE.NOT_FOUND })

const send500 = (error, res) => {
  logger.error(error)

  return res.status(500).end()
}

module.exports = {
  ERROR_CODE,
  send200,
  send400,
  send401,
  send404,
  send500,
}
