const express = require('express')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser')
const passport = require('passport')

const logger = require('./config/logger.config')
const authRouter = require('./routes/auth.route')
const taskRouter = require('./routes/task.route')
const checkAuth = require('./middlewares/auth.middleware')
const initPassport = require('./config/passport.config')
const connectMongo = require('./models')

const app = express()
app.use(morgan('dev'))
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(passport.initialize())

initPassport()
connectMongo()

app.use('/api/auth', authRouter)
app.use('/api', checkAuth(), taskRouter)

const PORT = 3000

app.listen(PORT, () => {
  logger.info(`Server started on port ${PORT}`)
})
