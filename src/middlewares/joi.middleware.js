const Joi = require('joi')
const { send400 } = require('../config/error.config')

const createTaskSchema = Joi.object({
  title: Joi.string().trim().min(3).max(80)
    .required(),
  description: Joi.string().trim().min(3).max(80)
    .required(),
})

const doneTaskSchema = Joi.object({
  title: Joi.string().trim().min(3).max(80)
    .required(),
})

const loginUserSchema = Joi.object({
  email: Joi.string().trim().min(3).required(),
  password: Joi.string().trim().min(6).required(),
  firstName: Joi.string().trim().optional(),
  lastName: Joi.string().trim().optional(),
})

const JoiValidateRequestSchema = (schema) => (req, res, next) => {
  const { error } = schema.validate(req.body)

  if (error) {
    return send400(res, 'error', error.message)
  }

  return next()
}

module.exports = {
  JoiValidateRequestSchema,
  loginUserSchema,
  createTaskSchema,
  doneTaskSchema,
}
