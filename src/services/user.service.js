const { ERROR_CODE } = require('../config/error.config')
const UserModel = require('../models/user.model')

const getUser = async (email, password) => {
  const user = await UserModel.findOne({ email })

  if (!user) {
    return null
  }

  const isValidPass = await user.isPassValid(password)

  if (!isValidPass) {
    throw Error(ERROR_CODE.PASS_NOT_VALID)
  }

  return user
}

const createUser = async (email, password, firstName, lastName) => {
  const user = await UserModel.create({
    email, password, firstName, lastName,
  })

  return user
}

module.exports = {
  createUser,
  getUser,
}
