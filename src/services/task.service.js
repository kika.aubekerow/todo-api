const { ERROR_CODE } = require('../config/error.config')
const TaskModel = require('../models/task.model')

const addTask = async (userId, title, description) => {
  const task = await TaskModel.create({ userId, title, description })

  return task
}

const removeTask = async (userId, title) => {
  const task = await TaskModel.findOneAndDelete({ userId, title })

  if (!task) {
    return null
  }

  return task
}

const markTaskDone = async (userId, title) => {
  const task = await TaskModel.findOne({ userId, title })

  if (!task) {
    return null
  }

  if (task.done) {
    throw Error(ERROR_CODE.TASK_ALREADY_DONE)
  }

  task.done = true
  await task.save()

  return task
}

const changeTask = async (userId, title, newTitle, description) => {
  const isTitleExist = await TaskModel.findOne({ userId, title: newTitle })

  if (isTitleExist) {
    throw Error(ERROR_CODE.TITLE_NOT_UNIQUE)
  }

  const task = await TaskModel.findOneAndUpdate(
    { userId, title },
    { title: newTitle, description },
    { new: true },
  )

  if (!task) {
    return null
  }

  return task
}

const getTaskList = async (userId, isDone = false) => {
  const taskList = await TaskModel.find({ userId, done: isDone })

  return taskList
}

module.exports = {
  addTask,
  removeTask,
  markTaskDone,
  changeTask,
  getTaskList,
}
